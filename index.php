<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");


$sheep = new Animal("shaun");

echo "Nama : " . $sheep->name."<br>"; // "shaun"
echo "Jumlah kaki = " . $sheep->legs. "<br>"; // 4
echo "Berdarah dingin? " . $sheep->cold_blooded. "<br><br>"; // "no"

$monyet = new Kera("kera sakti");

echo "Nama : " . $monyet->name."<br>"; // "shaun"
echo "Jumlah kaki = " . $monyet->legs. "<br>"; // 4
echo "Berdarah dingin? " . $monyet->cold_blooded. "<br>"; // "no"
echo $monyet->Yelling("AAUUUOO");

$kodok = new Kodok("buduk");

echo "<br><br>Nama : " . $kodok->name."<br>"; // "shaun"
echo "Jumlah kaki = " . $kodok->legs. "<br>"; // 4
echo "Berdarah dingin? " . $kodok->cold_blooded. "<br>"; // "no"
echo $kodok->Jumping("Hop Hop");
?>